Metadata-Version: 2.1
Name: ginga
Version: 3.2.0
Summary: A scientific image viewer and toolkit
Home-page: https://ejeschke.github.io/ginga/
Author: Ginga Maintainers
Author-email: eric@naoj.org
License: BSD
Description: Ginga is a toolkit designed for building viewers for scientific image
        data in Python, visualizing 2D pixel data in numpy arrays.
        The Ginga toolkit centers around an image display class which supports
        zooming and panning, color and intensity mapping, a choice of several
        automatic cut levels algorithms and canvases for plotting scalable
        geometric forms.  In addition to this widget, a general purpose
        'reference' FITS viewer is provided, based on a plugin framework.
        
        Ginga is distributed under an open-source BSD licence. Please see the
        file LICENSE.txt in the top-level directory for details.
        
Keywords: scientific,image,viewer,numpy,toolkit,astronomy,FITS
Platform: UNKNOWN
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: BSD License
Classifier: Operating System :: MacOS :: MacOS X
Classifier: Operating System :: Microsoft :: Windows
Classifier: Operating System :: POSIX
Classifier: Programming Language :: C
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Scientific/Engineering :: Astronomy
Classifier: Topic :: Scientific/Engineering :: Physics
Requires-Python: >=3.7
Description-Content-Type: text/plain
Provides-Extra: recommended
Provides-Extra: test
Provides-Extra: docs
Provides-Extra: gtk3
Provides-Extra: qt5
Provides-Extra: tk
Provides-Extra: web
